* Muy importante
+ Importante
- Menos importante

-------
Worklog
-------

Dom 08-06-03

+ INPUTLINE: Ignorar pulsaci�n de ALT+Letra 
- INPUTLINE: Ctrl+C, Ctrl+X, Ctrl+V y portapapeles
+ MENU: Permitir ejecutar hooks al activar opciones
+ TABLE/LIST: "clipping" a nivel de columna 
- TABLE/LIST: Funcionalidad addline 
+ BUTTON: Responder a la letra precedida por &
- TOOLBAR: Alineaci�n izquierda/derecha/centro
- TOOLBAR: Capturar ALT+Letra globalmente y lanzar el men� correspondiente

Lun 09-06-03

+ Drop-down control (usando pop-up control)
+ General: CONTROL_INFO (WIDTH, HEIGHT, etc)
- Menu shadow (like a window)
+ WINDOW: Ventanas modales
+ Message Box (en el propio Fenix; ver GUI.INC)
- CONTROL_RESIZE
+ MENU: Navegaci�n por teclado (letra -> buscar & u opci�n que empiece)
- TOOLBAR/MENU: navegaci�n con teclado izquierda/derecha (!)
+ TABLE/LIST: TABLE_REMOVELINE(S), TABLE_MOVELINE(S), TABLE_SELECT[ED]

Mar 10-06-03

- TABLE/LIST: columna bitmap (array de ints para bitmap_get en lugar de strings)
+ TABLE/LIST: opci�n de hacer click en columnas
+ TABLE/LIST: opci�n de ordenar el contenido autom�ticamente o al hacer ese clic
- Tooltips (no son controles)

Lun 16-06-03

- TABLE/LIST: modo "preview" para mostrar miniaturas
- WINDOW: guardar datos definidos por el usuario (�una cadena? �un entero?)
- WINDOW: "Window type string" libre, para el usuario

Mar 17-06-03

- TABLE/LIST: adaptar ancho de columnas al control autom�ticamente
- Iconos de herramientas generales (para ventana FPG)
- Ventana FPG con visor de MAP y cuadro de di�logo de opciones (parcial)
- TOOL: two state buttons (checkbox o radio)
- General: Permitir detectar doble-clic (as� como otras acciones diversas)

Mie 18-06-03

- Drag & Drop support

Jue 19-06-03

- TABLE/LIST: Soporte de selecciones m�ltiples en modo normal
- TABLE/LIST: tecla -> lleva a la entrada siguiente que empiece por ella

Vie 20-06-03

- FPG: Reconvertido el TEST.PRG en un FPG.PRG con la IMAGE.DLL
- Opciones adicionales al FPG (carga de MAPs, borrar m�ltiples, etc)

Jue 03-07-03

- Soporte clipboard (COPY_TEXT/GRAPH, PASTE_TEXT/GRAPH) a falta de
  puntos de control y datos adicionales del mapa

Lun 07-07-03

- Esqueleto de editor gr�fico

Mar 08-07-03

- Edici�n de puntos de control en editor gr�fico (integrado en utilidad FPG)

Lun 21-07-03

- Men� principal adaptable al tipo de ventana actual

---------
POR HACER
---------


Bugs:

- INPUTLINE: Revisar mays dcha + movimiento del cursor
- WINDOW: Requiere test intensivo del algoritmo de redimensionamiento
- El profiler mete datos imposibles cuando se ejecuta el TEST.PRG
- DROPDOWN: Queda pulsado en ocasiones tras hacer la selecci�n
- MENU: saltar las opciones deshabilitadas al desplazar los cursores
- Falta poder redimensionar ventanas por la izquierda
- Revisi�n general de ENTER/LEAVE, parece que falle ej. botones de sistema

Features de la utilidad FPG:

- Propiedades de m�ltiples ficheros dentro de un FPG
- Portapapeles: copiar, cortar y pegar gr�ficos (simple y m�ltiples)
- Opci�n de cargar la paleta de un gr�fico nuevo o mezclarla con la actual
- Men� contextual en mapas dentro de un FPG (�duplicar, borrar...?)

Features de la GUI.DLL:

- Soporte de portapapeles gr�fico, para poder copiar y pegar bitmaps
- TABLE/LIST: redimensionamiento de columna no debe mover scroll vertical
- TABLE/LIST: revisi�n general del modo preview, especialmente con multiselect
- WINDOW: men� contextual con opciones de abrir/cerrar/maximizar/etc
+ WINDOW: al redimensionar, evitar colisi�n entre controles
- MENU: Gr�ficos a la izquierda de las opciones
- MENU: Teclas de acceso r�pido a la derecha de las opciones
- MENU: MENU_RENAME, MENU_REMOVE, MENU_MOVE
- General: los procesos lanzados por BUTTON, MENU_ACTION, etc, deber�an
	recibir alg�n par�metro (texto del bot�n/acci�n, id del control, etc)
- Opciones generales para no dibujar sombras p.ej.
- WINDOW: mover foco al principio tras HIDE/SHOW (?)
- INPUTLINE: Undo

Desplazadas hasta nuevas versiones:

- TABLE/LIST: columnas ocultables
- TABLE/LIST: men� contextual para mostrar/ocultar columnas
- TABLE/LIST: columnas unidas (para poder hacer bitmap + texto en 1 columna)
- TABLE/LIST: columnas editables (usar un INPUT embebido?)
- General: permitir a�adir se�ales a los controles (onchange, onclick, etc)
- Multipage (pseudo-contenedor) para ofrecer varias vistas o similar)
- Barras de scroll de tipo "fixed"

Gr�ficos:

- Iconos para cada fichero
- Iconos para cuadros de mensaje
- Iconos para herramientas (a�adir, borrar, herramientas de edici�n bitmap...)

Cuadros de di�logo:

- Selecci�n de color (paleta/grid)

Nuevos controles:

- Progress bar
- Split view
- Spin Buttons
- Combo boxes (usar container con un input + un tool)
- Tree Control (usar una extensi�n de TABLE)
- Directory List (TABLE o TREE)
- Editor multi-l�nea
- Color grid

Future features:

- Ventanas con caching de contenido en un bitmap
- Ventanas transl�cidas realmente (gr_text_put con alpha!)
- Navegaci�n por teclas de cursor adem�s de TAB/SHIFT-TAB
- Icono de ventana
- Dibujo de controles temificable con rutinas made in the user
- Controles deshabilitados (checkboxes, botones, inputs...)

------------------------------------------------------------------------
Descripci�n del funcionamiento del nuevo algoritmo de redimensionamiento
------------------------------------------------------------------------

La idea b�sica consiste en la creaci�n de l�neas de tensi�n. Una l�nea
de tensi�n une dos controles cercanos, o bien un control con uno de los
bordes de la ventana, de manera que establece una relaci�n de distancia
inamovible.

Las l�neas de tensi�n crean una relaci�n de dependencia, de manera
que se establecen series de l�neas. Por ejemplo, una l�nea fuerza un
control a unirse al borde inferior de la ventana, pero otra l�nea
fuerza al control a no separarse de otro control, el cual a su vez
est� unido al borde superior de la ventana. Al desplazar el borde
inferior, las l�neas deben permanecer est�ticas, por lo que al menos
uno de estos dos controles debe variar de tama�o. La rutina debe ser
lo bastante inteligente como para distribuir el cambio de tama�o
entre ambos controles de forma �ptima.

El funcionamiento en l�neas generales del algoritmo de redimensionado 
de una ventana es:

- Buscar todas las l�neas en contacto con el borde desplazado

- Por cada l�nea encontrada, establecer una serie de l�neas entre
  los controles relacionados. Por cada control del que parten
  dos o m�s l�neas adicionales, crear una nueva serie entera.

- Por cada control, crear un array de valores con el tama�o
  asignado actualmente.

- Ordenar las series por orden de prioridad. Las series con m�s
  limitaciones de tama�o entre los controles van primero.

- Por cada serie, repartir el valor de desplazamiento total entre
  los controles redimensionados. Si un control ya tiene un valor
  elegido de tama�o, dividir lo que queda entre resto.

- Recorrer cada serie desplazando los controles que aparecen
  en ella a sus nuevas posiciones.

Actualmente las series de l�neas (paths) no se ordenan, sino que el
programa coge una cualquiera.
