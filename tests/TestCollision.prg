/*
 *	Prueba de chequeo de colisi�n
 *	-----------------------------
 *	Las posiciones, tama�o y flags de los gr�ficos son aleatorias.
 *	Pueden arrastrarse gr�ficos con el rat�n para hacer pruebas
 *	exhaustivas.
 */

PROGRAM TestCollision;

GLOBAL
	INT cogido = 0;		// ID del proceso "cogido" por el rat�n

// Proceso a ejecutar por cada gr�fico

PROCESS TestGraphic(GRAPH)
PRIVATE
	INT I;
	INT BX = -1, BY;
BEGIN
	// El padre debe ejecutarse primero, para borrar los textos que
	// los hijos hayan escrito el frame anterior
	PRIORITY--;

	LOOP
		// Pone el gr�fico en una posici�n aleatoria
		X = RAND(0,640);
		Y = RAND(0,380);
		FLAGS = RAND(0,7);
		SIZE = RAND(100,200);
		ANGLE = 0;

		// Pone una cruz sobre su centro inicial en el fondo
		DRAWING_MAP (0, 0);
		DRAWING_COLOR (RGB(182,64,64));
		DRAW_LINE (X-60,Y,X+60,Y);
		DRAW_LINE (X,Y-60,X,Y+60);

		// Cuando se pulse 0, se volver� a colocar el gr�fico
		// en una nueva posici�n aleatoria
		WHILE ASCII != 48:

			// Los cursores giran el gr�fico
			IF KEY(_RIGHT): ANGLE += RAND(1000,2000); END
			IF KEY(_LEFT): ANGLE -= RAND(1000,2000); END
			IF KEY(_S): SIZE = 100; END

			// Si el gr�fico ha sido cogido por el rat�n...
			IF BX != -1:
				IF MOUSE.LEFT:
					// ... actualiza su posici�n ...
					X += MOUSE.X - BX;
					Y += MOUSE.Y - BY;
					BX = MOUSE.X;
					BY = MOUSE.Y;
				ELSE
					// ... o es soltado
					cogido = 0;
					BX = BY = -1;
				END
			END

			// Comprueba colisi�n con el rat�n
			I = COLLISION(TYPE MOUSE);
			IF I:
				WRITE (0, X, Y, 4, "Mouse");
				IF MOUSE.LEFT AND cogido == 0:
					// El usuario "coge" el gr�fico, pero
					// s�lo se permite uno a la vez
					BX = MOUSE.X;
					BY = MOUSE.Y;
					cogido = ID;
				END
			END

			// Comprueba otras colisiones
			I = COLLISION(TYPE TestGraphic);
			IF I:  WRITE (0, X, Y+8, 4, I); END
			FRAME;
		END
		FRAME;
		ASCII = 0;
	END
END

BEGIN
	// Inicializa el modo gr�fico
	FULL_SCREEN = FALSE;
	GRAPH_MODE = MODE_16BITS;
	SET_MODE (M640x480);
	SET_FPS (0, 0);

	// Pone un cursor al rat�n
	MOUSE.GRAPH = LOAD_PNG ("Cursor.png");
	SET_CENTER (0, MOUSE.GRAPH, 1, 1);

	// Crea diversos gr�ficos de prueba
	GRAPH = LOAD_PNG("Check16.png");
	SET_CENTER (0, GRAPH, 24, 24);
	FROM X = 1 TO 20: TestGraphic(GRAPH); END
	GRAPH = 0;

	// Borra el fondo y los textos de los procesos hijos a cada frame
	LOOP 
		DELETE_TEXT(0); 
		WRITE (0, 16, 448, 0, "Prueba del chequeo de colisiones");
		WRITE (0, 16, 460, 0, "Rat�n: arrastrar | Cursores: Rotar | S: Reinicar tama�os | 0: Posiciones aleatorias | ESC: Salir");
		IF ASCII == 48: MAP_CLEAR (0, 0, 0); END
		IF KEY(_ESC): EXIT(); END
		FRAME; 
	END
END
